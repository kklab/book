# News:

* Preesentation on "[Forking the book: an open invitation to translate Aesthetic Programming](https://pretalx.coscup.org/coscup-2023/talk/review/MJTKBGMV88ETAJSVQDLLPR9JUUFWFZBQ)" at COSCUP 2023, Taipei  
* [Call for Participation CCSWG ’22](https://www.centreforthestudyof.net/?p=6045), the 7th biennial Critical Code Studies Working Group, Jan 15 – Feb 14, 2022.
*  Mark C. Marino. "Aesthetic Programming teaches programming to critical coders," published online 2021. [DOI: 10.1080/09502386.2021.1993291](https://www.tandfonline.com/doi/full/10.1080/09502386.2021.1993291), published in *Cultural Studies* Volume 36, 2022 - Issue 6.
* Sarah Ciston & Mark C. Marino, 'How to Fork a Book: The Radical Transformation of Publishing'. Medium (blog), 19 August 2021. https://markcmarino.medium.com/how-to-fork-a-book-the-radical-transformation-of-publishing-3e1f4a39a66c.
* David Young. “Theorising while() Practising: A Review of Aesthetic Programming.” *Computational Culture* 8 July 2021. http://computationalculture.net/theorising-while-practising-a-review-of-aesthetic-programming/.

# Events:

* 10-11.May.2022 - [Talk](https://www.mediacoop.uni-siegen.de/en/events/introducing-aesthetic-programming-lecture-with-winnie-soon-geoff-cox-london-south-bank-university-aarhus-university/) and [workshop](https://www.mediacoop.uni-siegen.de/en/events/forking-translating-aesthetic-programming-workshop-with-winnie-soon-geoff-cox-london-south-bank-university-aarhus-university/) on Introducing/Forking/Translating Aesthetic Programming, CRC Media of Cooperation (University of Siegen), Germany
* 7-13.Feb.2022 - Led the [online discussion of Critical Code Studies Working Group](https://wg.criticalcodestudies.com/index.php?p=/categories/2022-week-4) on Aesthetic Programming (with Lee Tzu Tung, Ren Yu, Shih-yu)
* 18.Nov.2021 - Exhibiting *Aesthetic Programming* as part of [Data Vitality: Soft Infrastructures and Economies of Knowledge](https://www.aalto.fi/en/datavitality), Dipoli Gallery, Finland (curated by Edel O' Reilly)
* 6.Oct.2021 - [Aesthetic Programming](https://twitter.com/TiP_itu/status/1445691776442896385?s=20), [Technologies in Practice](https://tip.itu.dk/), IT University of Copenhagen
* 25.Jun.2021- Workshop and Talks on Aesthetic Programming, [Transart Institute](https://www.transartinstitute.org/intensives-202021-session-8)
* 17.Jun.2021- Book launch: Aesthetic Programming, Coding Literacy, Practices & Cultures:
[A networked series of research](colloquiuahttps://danvers.github.io/oncoding/), University of Magdeburg & Film University KONRAD WOLF in Potsdam Babelsberg
* 21.May.2021- [Book launch: Aesthetic Programming](https://www.youtube.com/watch?v=rGwLPWMtQVk), HaCCS Lab, University of Southern California
* 23.Apr.2021- Book Launch: Aesthetic Programming, DARC, Aarhus University

# Logs (major web updates):
* 25.Aug.2022 - update the 2022 showcase
* 19.Jan.2022 - web hosting and domain name changes from Hong Kong to Europe with a much faster connection. We are using GreenGeeks web hosting.
* 3.Sep.2021 - update the 2021 showcase, add the news section and update Vocable Code

# Planning/Working list:
* Update Vocable code
* A documentation on all customized styling syntax in markdown pages (hosting your own customized fork of the interactive web platform)
* [Chinese translation(forking)](https://hackmd.io/team/aesthetic-programming) of Aesthetic Programming

# Known issues

* Chapter 4 sample code with the mic/sound issue: getLevel() :
  - The sample code in chapter 4 needs to run directly on GitLab with the lower version of the p5 libraries - [here](https://aesthetic-programming.gitlab.io/book/). (The web version sketch is now using the latest libraries-2022-by default) See the relevant open issue [here](https://github.com/processing/p5.js-sound/issues/499). The sketch with the latest library can run on Firefox ESR, Mac Google Chrome but just not on Firefox ver 96.
* There is a bug in which hyperlink ends with '/' cannot be processed in both miniX and project lists, etc.
